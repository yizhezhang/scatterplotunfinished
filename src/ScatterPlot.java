
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * @author Chuck Cusack, January 2013, February 2015
 */
public class ScatterPlot {
	// The desired dimensions of the main window
	private int width = 600;
	private int height = 600;
	private ArrayList<ColoredPoint> points = null;
	private JFrame theFrame;
	private ColoredPointPanel pointPanel;
	JFileChooser fc = null;

	public ScatterPlot() {
		super();
		makeFrame();
	}

	public ScatterPlot(int width, int height) {
		super();
		this.width = width;
		this.height = height;
		makeFrame();
	}

	/**
	 * The main method, which allows us to run the application without using a
	 * webpage. In other words, this is the method that is called when you run a
	 * Java application.
	 */
	public static void main(String[] args) {
		new ScatterPlot();
	}

	/**
	 * Create the drawing panel and various other components that we need for
	 * our program.
	 */
	public void makeFrame() {
		theFrame = new JFrame();
		theFrame.setTitle("Scatter plot");
		theFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Place all of the graphical components on the main window
		Container cont = theFrame.getContentPane();
		cont.setLayout(new BorderLayout());

		// Start with random data.
		points = ColoredPointUtil.generateRandomList(100, this.width,
				this.height);

		pointPanel = new ColoredPointPanel(points);
		cont.add(pointPanel, BorderLayout.CENTER);

		// Add a menu bar and items.
		// The File menu
		JMenu fileMenu = new JMenu("File");

		JMenuItem exitMenuItem = new JMenuItem("Exit");
		exitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(exitMenuItem);

		JMenuItem openMenuItem = new JMenuItem("Open File...");
		openMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				readData();
			}
		});
		fileMenu.add(openMenuItem);

		JMenuItem saveMenuItem = new JMenuItem("Save...");
		saveMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				writeData();
			}
		});
		fileMenu.add(saveMenuItem);
		
		JMenuItem newMenuItem = new JMenuItem("New...");
		newMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generateRandomList();
			}
		});
		fileMenu.add(newMenuItem);

		// The Help Menu
		JMenu helpMenu = new JMenu("Help");
		JMenuItem aboutMenuItem = new JMenuItem("About");
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(theFrame,
						"Scatter Plot\nImplemented by Chuck Cusack");
			}
		});
		helpMenu.add(aboutMenuItem);

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		theFrame.setJMenuBar(menuBar);

		// Finish setting up the main window
		theFrame.setBackground(Color.white);
		theFrame.pack();
		// Make it a little bigger to take into account the borders of the
		// frame.
		theFrame.setSize(new Dimension(width + 20, height + 40));
		theFrame.setVisible(true);
	}

	private void readData() {
		if (fc == null) {
			fc = new JFileChooser();
		}
		int returnVal = fc.showDialog(theFrame, "Open");

		// We check whether or not they clicked the "Open" button
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			// We get a reference to the file that the user selected.
			File file = fc.getSelectedFile();
			// Make sure it actually exists.
			if (!file.exists()) {
				showFileNotFoundDialog();
			} else {
				try {
					points = ColoredPointUtil.readList(file);
					pointPanel.setPoints(points);
				} catch (FileNotFoundException e) {
					showFileNotFoundDialog();
				} catch (IOException e) {
					showIOErrorDialog();
				}
			}
		}
	}

	private void writeData() {
		if (fc == null) {
			fc = new JFileChooser();
		}
		int returnVal = fc.showDialog(theFrame, "Save");

		// We check whether or not they clicked the "Open" button
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			// We get a reference to the file that the user selected.
			File file = fc.getSelectedFile();
			// Make sure it actually exists.
			if (!file.exists() || askAboutOverwriting()) {
				if (!ColoredPointUtil.writeList(file, points)) {
					showIOErrorDialog();
				}
			}
		}
	}

	private void generateRandomList() {
		String userinput = JOptionPane.showInputDialog("How many Points?");
		try {
			int size = Integer.parseInt(userinput);
			points = ColoredPointUtil.generateRandomList(size, width, height);
			pointPanel.setPoints(points);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(theFrame, "Please enter an integer",
					"Input error!", JOptionPane.ERROR_MESSAGE);
		}
	}

	private boolean askAboutOverwriting() {
		int result = JOptionPane.showConfirmDialog(theFrame,
				"Files Exists.  Overwrite it?", "Overwrite?",
				JOptionPane.YES_NO_OPTION);
		return result == JOptionPane.YES_OPTION;
	}

	private void showFileNotFoundDialog() {
		JOptionPane.showMessageDialog(theFrame, "That file does not exist!",
				"File not found", JOptionPane.ERROR_MESSAGE);
	}

	private void showIOErrorDialog() {
		JOptionPane.showMessageDialog(theFrame,
				"Problem reading/writing the file.", "I/O Error",
				JOptionPane.ERROR_MESSAGE);
	}
}
