import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * A utility class that reads and writes data related to ColoredPoints.
 * 
 * Some of the things that are worth pointing out:
 * 
 * -- It demonstrates two ways of dealing with checked exceptions
 * 
 * -- Add a throws clause to the method to specify that the calling class needs
 * to deal with exceptions
 * 
 * -- Not add a throws clause and deal with the exceptions in the method.
 * 
 * In practice you would probably be consistent and have all relevant methods do
 * one or the other and not mix. Here they are mixed so you can see the
 * differences between the two ways of dealing with exceptions.
 * 
 * 
 * ************ Not fully implemented! ********************
 * 
 * @author cusack (Some code borrowed from Alayna Ruberg's solution) Modified
 *         2/5/15, CAC.
 * 
 */
public class ColoredPointUtil {

	/**
	 * Generates a random list of colored points. The list will have size points
	 * whose x coordinates are between 0 and width and y coordinates are between 0
	 * and height. The colors of the points will be random.
	 * 
	 * @param size
	 *            the number of points to generate
	 * @param width
	 *            the maximum x value of each point
	 * @param height
	 *            the maximum y value of each point
	 * @return an ArrayList of the points.
	 */
	public static ArrayList<ColoredPoint> generateRandomList(int size, int width, int height) {
		Random random = new Random(new Date().getTime());
		ArrayList<ColoredPoint> points = new ArrayList<ColoredPoint>();
		for (int i = 0; i < size; i++) {
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int red = random.nextInt(256);
			int green = random.nextInt(256);
			int blue = random.nextInt(256);
			ColoredPoint cp = new ColoredPoint(x, y, red, green, blue);
			points.add(cp);
		}
		return points;
	}

	/**
	 * Read a list of ColoredPoints that are stored in a text file, one per line.
	 * Each line should be parsed with the stringToColoredPoint method.
	 * 
	 * Notice that this method states that it throws several exceptions. In this
	 * case we have decided that if errors occur that we want the calling class to
	 * have to deal with them.
	 * 
	 * @param filename
	 *            the name of the file that contains the points.
	 * @return An ArrayList of ColoredPoints
	 * @throws FileNotFoundException
	 *             , IOException if something goes wrong with the file I/O.
	 */
	public static ArrayList<ColoredPoint> readList(File filename) throws FileNotFoundException, IOException {
		ArrayList<ColoredPoint> points = new ArrayList<ColoredPoint>();
		// IMPLEMENT ME!
		// Hint: You will read one line at a time and then parse each line by
		// calling a method in this class.
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line = reader.readLine();
		while (line != null) {
			points.add(stringToPoint(line));
			line = reader.readLine();
		}
		return points;
	}

	/**
	 * Write a list of ColoredPoints, one per line. Each line should be written
	 * using ColoredPoint's toString method.
	 * 
	 * Notice that this method does NOT have a throws clause. In this case we are
	 * saying that we are handling any checked exceptions that might occur.
	 * 
	 * @param filename
	 *            The file to write to
	 * @param points
	 *            The list of points we want to write out to the file.
	 * @returns true if it correctly wrote the file and false otherwise.
	 */
	public static boolean writeList(File file, ArrayList<ColoredPoint> points) {
		// IMPLEMENT ME
		// Hint: You will be outputting strings one line at a time.
		// What I/O class(es) would be best for this?
		// Note that you will use one of the other methods in this class to help
		// you.

		// Since this isn't finished yet we didn't write, so return false.
		try (FileWriter writer = new FileWriter(file);) {
			for (int i = 0; i < points.size(); i++) {
				writer.write(points.get(i).toString());
				writer.write('\n');
			}
			return true;
		} catch (IOException e) {
			System.err.println("There was a problem writing to " + file.getName());
		}
		return false;
	}

	/**
	 * 
	 * @param pointAsString
	 *            The string representation of the point in the format specified in
	 *            the ColoredPoint.toString() method.
	 * @return A ColoredPoint constructed from the string.
	 * 
	 */
	public static ColoredPoint stringToPoint(String pointAsString) {
		// IMPLEMENT ME!
		// You may want to use the split method on String (maybe more than once,
		// even).

		// This line is here just so the class compiles
		String[] list = pointAsString.split(":");
		String[] cordinates = list[0].split(",");
		String[] colors = list[1].split(" ");

		int x = Integer.parseInt(cordinates[0]);
		int y = Integer.parseInt(cordinates[1]);

		int red = Integer.parseInt(colors[0]);
		int green = Integer.parseInt(colors[1]);
		int blue = Integer.parseInt(colors[2]);
		return new ColoredPoint(x, y, red, green, blue);
	}
}
